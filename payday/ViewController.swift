//
//  ViewController.swift
//  payday
//
//  Created by Martin Grant on 14/03/2019.
//  Copyright © 2019 Martin Grant. All rights reserved.
//

import UIKit


extension Double {
    /// Rounds the double to decimal places value
    func rounded(toPlaces places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}


class ViewController: UIViewController, UIPickerViewDelegate, UIPickerViewDataSource {
    
    
    var pickerData: [String] = [String ]()
    
    @IBOutlet weak var picker: UIPickerView!
    //@IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet var topview: UIView!
    
    // text fields
    @IBOutlet weak var grossSalaryTextField: UITextField!
    
    // calculator labels
    @IBOutlet weak var grossPayLabel: UILabel!
    @IBOutlet weak var netPayLabel: UILabel!
    @IBOutlet weak var taxableLabel: UILabel!
    @IBOutlet weak var taxedLabel: UILabel!
    @IBOutlet weak var niLabel: UILabel!
    
    // pay date label
    @IBOutlet weak var paydatelabel: UILabel!
    @IBOutlet weak var untilPayday: UILabel!
    
    // accumulator label
    @IBOutlet weak var earnedTodayLabel: UILabel!
    @IBOutlet weak var earnedTotalLabel: UILabel!
    @IBOutlet weak var earnedWarningLabel: UILabel!
    
    
    var workingDaysPerYear = 260.0
    var payDate = 0
    
    
    let payCalculator = PayCalculator()
    
    
    
 
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        // set on-screen keyboard to numeric input only
        self.grossSalaryTextField.keyboardType = UIKeyboardType.decimalPad
        
        
        grossSalaryTextField.addTarget(self, action: #selector(textFieldDidChange(sender:)), for: .editingChanged)
        
        //scrollview.contentSize = CGSize(width: scrollview.contentSize.width, height: 2000)
        
        let defaults = UserDefaults.standard
        payDate = defaults.integer(forKey: "payDate")
        
        payCalculator.loadDefaults()
        
        setupPickerWheel()
        
        calculate(grossPay: payCalculator.grossPay)
        calcDaysUntilPayday(date: payDate)
    
    
        
        /*var contentRect = CGRect.zero
        
        for view in scrollView.subviews {
            contentRect = contentRect.union(view.frame)
        }
        scrollView.contentSize = contentRect.size
        scrollView.isDirectionalLockEnabled = true;*/
    }
    
    
    func setupPickerWheel() {
        self.picker.delegate = self
        self.picker.dataSource = self
        
        for i in 1...31 {
            pickerData.append(String(i))
        }
        
        picker.selectRow(payDate - 1, inComponent: 0, animated: true)
    }
    
    
    func updateCalculatorValues() {
        grossSalaryTextField.text = String(payCalculator.grossPay.rounded(toPlaces: 2))
        grossPayLabel.text = String(payCalculator.grossPay.rounded(toPlaces: 2))
        taxableLabel.text = String(payCalculator.taxableIncome.rounded(toPlaces: 2))
        taxedLabel.text = String(payCalculator.taxed.rounded(toPlaces: 2))
        niLabel.text = String(payCalculator.ni.rounded(toPlaces: 2))
        netPayLabel.text = String(payCalculator.netPay.rounded(toPlaces: 2))
    }
    
    
    func calculate(grossPay: Double) {
        payCalculator.grossPay = grossPay
        payCalculator.calculate()
    
        // label displays
        updateCalculatorValues()
        
        payCalculator.saveDefaults()
        
        calcAccumulator()
        
        //self.view.endEditing(true) // hide keyboard
    }
    
    
    @objc func textFieldDidChange(sender: UITextField) {
        let grossPay = Double(grossSalaryTextField.text!) ?? 0
        calculate(grossPay: grossPay)
    }
  
 

    
    func displayWarning(title: String, message: String) {
        let alertController = UIAlertController(title: title, message: netPayLabel.text, preferredStyle: UIAlertController.Style.alert)
        
        alertController.addAction(UIAlertAction(title: message, style: UIAlertAction.Style.default, handler: nil))
        
        present(alertController, animated:true, completion: nil)
    }
    
    
    
    
    
    
    
    
    

    
    func calcDaysUntilPayday(date: Int) {
        let calendarDate = Calendar.current.dateComponents([.day, .year, .month], from: Date())
        
        let payDate = Int(date - calendarDate.day!)
        paydatelabel.text = String(payDate)
        
        paydatelabel.isHidden = false
        untilPayday.isHidden = false
        
        
        let defaults = UserDefaults.standard
        defaults.set(date, forKey: "payDate")
    }
    
    
    
    
    
    
   
    
    
    func calcAccumulator() {
        let calendarDate = Calendar.current.dateComponents([.day, .year, .month], from: Date())
        let today = calendarDate.day
        
        
        let hour = 13//Calendar.current.component(.hour, from: Date())
        
        let dailyRate = payCalculator.netPay / workingDaysPerYear
        let hourlyRate = dailyRate / 8.0
        
        if (hour >= 9 && hour <= 17) {
            let begin = 9.0
            //let end = 17.0
            
            let hoursIntoShift = Double(hour) - begin
            
            earnedWarningLabel.text = String(hoursIntoShift) + " hours into shift"
            let earnedToday = hourlyRate * hoursIntoShift
            earnedTodayLabel.text = String(earnedToday.rounded(toPlaces: 2))
            let earnedTotal = (dailyRate * Double(today ?? 1)) + earnedToday
            earnedTotalLabel.text = String(earnedTotal.rounded(toPlaces: 2))
            
        } else {
            earnedWarningLabel.text = "must be 9-5"
        }
    }
    
    
    
    
    
    
    
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return pickerData.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return pickerData[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let selectedPayDate = pickerData[row] as String
        calcDaysUntilPayday(date: Int(selectedPayDate) ?? 1)
        calcAccumulator()
    }
}
