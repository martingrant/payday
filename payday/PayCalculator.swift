//
//  PayCalculator.swift
//  payday
//
//  Created by Martin Grant on 25/04/2019.
//  Copyright © 2019 Martin Grant. All rights reserved.
//

import Foundation

class PayCalculator {
    
    var grossPay = 0.0
    var netPay = 0.0
    var personalAllowance = 12500.0
    var firstIncomeTaxRate = 0.2
    var secondIncomeTaxAmount = 46351.0
    var secondIncomeTaxRate = 0.4
    var thirdIncomeTaxAmount = 150000.0
    var thirdIncomeTaxRate = 0.45
    var taxed = 0.0
    var taxableIncome = 0.0
    var niAllowance = 8632.0
    var niRate = 0.12
    var ni = 0.0
    
    init() { // Constructor
        print("constructed")
    }
    
    
    func loadDefaults() {
        let defaults = UserDefaults.standard
        grossPay = defaults.double(forKey: "grossPay")
        taxableIncome = defaults.double(forKey: "taxable")
        taxed = defaults.double(forKey: "tax")
        ni = defaults.double(forKey: "ni")
        netPay = defaults.double(forKey: "netPay")
    }
    
    
    func saveDefaults() {
        let defaults = UserDefaults.standard
        defaults.set(grossPay, forKey: "grossPay")
        defaults.set(taxableIncome, forKey: "taxable")
        defaults.set(taxed, forKey: "tax")
        defaults.set(ni, forKey: "ni")
        defaults.set(netPay, forKey: "netPay")
    }
    
    
    func calculate() {
        taxableIncome = grossPay
        
        // no allowance on income greater than £125,000
        if (grossPay < 125000) {
            taxableIncome = grossPay - personalAllowance
        }
        
        taxed = calculateIncomeTax(val: taxableIncome)
        ni = calculateNI(val: grossPay)
        
        netPay = grossPay - taxed - ni
    }
    
    
    func calculateIncomeTax(val: Double) -> Double {
        var amount = 0.0
        
        if (val < 37500) {
            amount = val * 0.2
        }
        
        if (val > 37500 && val < 150000) {
            let amount1 = 37500 * 0.2
            let amount2 = (val - 37500) * 0.4
            
            amount = amount1 + amount2
        }
        
        if (val > 150000) {
            let amount1 = 37501 * 0.2
            let amount2 = (150000 - 37500) * 0.4
            let amount3 = (val - 150000) * 0.45
            
            amount = amount1 + amount2 + amount3
        }
    
        return amount
    }
    
    
    func calculateNI(val: Double) -> Double {
        var amount = 0.0
        
        
        if (val < 50042) {
            amount = (val - niAllowance) * niRate
        } else {
            var amt = (50042 - niAllowance) * niRate
            
            var amt2 = ((val - 50042)) * 0.02
            
            
            amount = amt + amt2
        }
        
        
        return amount
    }
}
